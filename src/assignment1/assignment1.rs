pub fn solution(s: String) -> &'static str {
    let mut count = 0;
    for (idx, c) in s.chars().enumerate() {
        if idx == 0 && c == 'R' {
            return "Bad boy"
        }
        count += match c {
            'S' => -1,
            'R' => {
                match count < 0 {
                    true => 1,
                    false => 0,
                }
            },
            _ => panic!("Invalid character"),
        };
    }
    if count < 0 {
        return "Bad boy"
    }
    "Good boy"
}
