pub mod assignment1;
pub mod assignment2;

#[cfg(test)]
mod tests {
    use assignment1::assignment1;
    use assignment2::assignment2;

    use super::*;

    #[test]
    fn it_works_assignment1() {
        #[derive(Clone)]
        struct TestCase {
            input: String,
            expected: &'static str,
        }

        let test_cases = vec![
            TestCase {
                input: "SRSSRRR".to_string(),
                expected: "Good boy",
            },
            TestCase {
                input: "RSSRR".to_string(),
                expected: "Bad boy",
            },
            TestCase {
                input: "SSSRRRRS".to_string(),
                expected: "Bad boy",
            },
            TestCase {
                input: "SRRSSR".to_string(),
                expected: "Bad boy",
            },
            TestCase {
                input: "SSRSRRR".to_string(),
                expected: "Good boy",
            }
        ];

        for t in test_cases {
            let result = assignment1::solution(t.input.clone());
            if result != t.expected {
                println!("input: {}, expected: {}, result: {}", t.input, t.expected, result);
            }
            assert_eq!(result, t.expected);
        }
    }

    #[test]
    fn it_works_assignment2() {

        // n = the number of chickens: u32
        // k = the length of the roof: u32
        // p = position: Vec<u32> 
        //
        struct TestCase {
            n: u32,
            k: u32,
            p: Vec<u32>,
            expected: u32,
        }

        let test_cases = vec![
            TestCase {
                n: 5,
                k: 5,
                p: vec![2, 5, 10, 12, 15],
                expected: 2,
            },
            TestCase {
                n: 6,
                k: 10,
                p: vec![1, 11, 30, 34, 35, 37],
                expected: 4,
            },
        ];

        for t in test_cases {
            let result = assignment2::solution(t.n, t.k, t.p.clone());
            if result != t.expected {
                println!("n: {}, k: {}, p: {:?}, expected: {}, result: {}", t.n, t.k, t.p, t.expected, result);
            }
            assert_eq!(result, t.expected);
        }
    }
}
