pub fn solution(n: u32, k: u32, p: Vec<u32>) -> u32{
    let mut max = 0;
    for num in p.clone() {
        let p_roof: (u32, u32) = (num, num + k);
        let mut count = 0;
        for num2 in p.clone() {
            if num2 >= p_roof.0 && num2 < p_roof.1 {
                count += 1;
                continue;
            }
        }

        if count > max {
            max = count;
        }
    }

    max
}
